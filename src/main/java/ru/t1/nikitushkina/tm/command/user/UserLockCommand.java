package ru.t1.nikitushkina.tm.command.user;

import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    public static final String NAME = "user-lock";

    public static final String DESCRIPTION = "Lock user by login.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN OF USER:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
