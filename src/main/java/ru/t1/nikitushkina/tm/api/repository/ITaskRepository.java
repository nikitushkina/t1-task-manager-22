package ru.t1.nikitushkina.tm.api.repository;

import ru.t1.nikitushkina.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>, IUserOwnedRepository<Task> {

    Task create(final String userId, String name, String description);

    Task create(final String userId, String name);

    List<Task> findAllByProjectId(final String userId, String projectId);

}
