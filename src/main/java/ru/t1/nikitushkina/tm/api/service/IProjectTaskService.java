package ru.t1.nikitushkina.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    void unbindTaskFromProject(String userId, String projectId, String taskId);

}
