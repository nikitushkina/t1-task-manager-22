package ru.t1.nikitushkina.tm.api.service;

import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByEmail(String email);

    User findByLogin(String login);

    Boolean isEmailExist(String email);

    Boolean isLoginExist(String login);

    void lockUserByLogin(String login);

    User removeByEmail(String email);

    User removeByLogin(String login);

    User setPassword(String id, String password);

    void unlockUserByLogin(String login);

    User updateUser(String id, String firstName, String lastName, String middleName);

}
