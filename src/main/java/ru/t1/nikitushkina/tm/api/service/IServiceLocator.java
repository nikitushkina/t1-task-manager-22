package ru.t1.nikitushkina.tm.api.service;

public interface IServiceLocator {

    IAuthService getAuthService();

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IUserService getUserService();

}
